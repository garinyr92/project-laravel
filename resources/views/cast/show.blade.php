@extends('layouts.master')

@section('title')
Cast
@endsection

@section('content-title')
Show Cast Data
@endsection

@section('content')
<div>
    <h2>Cast Data</h2>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{ $cast->nama }}" readonly>
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" value="{{ $cast->umur }}" readonly>
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Biodata</label>
        <textarea style="resize:none" rows="7" name="bio" class="form-control" readonly>{{ $cast->bio }}</textarea>
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <a href="{{ url('cast') }}" class="btn btn-secondary btn-sm">Kembali</a>
</div>
@endsection
