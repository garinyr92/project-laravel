@extends('layouts.master')

@section('title')
Cast
@endsection

@section('content-title')
Edit Cast
@endsection

@section('content')
<div>
    <h2>Edit Data</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama"
                value="{{ $cast->nama }}" required>
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur"
                value="{{ $cast->umur }}" required>
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Biodata</label>
            <textarea style="resize:none" rows="7" name="bio" class="form-control" placeholder="Masukkan biodata"
                required>{{ $cast->bio }}</textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <a href="{{ url('cast') }}" class="btn btn-secondary btn-sm">Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </form>
</div>
@endsection
