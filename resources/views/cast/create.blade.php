@extends('layouts.master')

@section('title')
Cast
@endsection

@section('content-title')
Create Cast
@endsection

@section('content')
<div>
    <h2>Tambah Data</h2>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama" required>
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur" required>
            @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Biodata</label>
            <textarea style="resize:none" rows="7" name="bio" class="form-control" placeholder="Masukkan biodata"
                required></textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection
