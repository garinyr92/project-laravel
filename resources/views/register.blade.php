<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SanberBook</title>
</head>

<body>
    <div>
        <h1>Buat Account Baru!</h1>
    </div>
    <div>
        <div>
            <h3>Sign Up Form</h3>
        </div>
        <form action="{{ url('/welcome') }}" method="post">
            @csrf
            <div>
                <label for="first_name">First Name</label> <br />
                <br />
                <input type="text" name="firstName" required />
            </div>
            <br />
            <div>
                <label for="last_name">Last Name</label> <br />
                <br />
                <input type="text" name="lastName" required />
            </div>
            <br />
            <div>
                <label for="gender">Gender</label> <br />
                <br />
                <input type="radio" name="gender" value="male" required />
                <label for="male">Male</label><br />
                <input type="radio" name="gender" value="female" />
                <label for="female">Female</label><br />
                <input type="radio" name="gender" value="other" />
                <label for="other">Other</label>
            </div>
            <br />
            <div>
                <label for="nationality">Nationality</label> <br />
                <br />
                <select name="nationality" required>
                    <option value="indonesia">Indonesia</option>
                    <option value="english">English</option>
                    <option value="other">Other</option>
                </select>
            </div>
            <br />
            <div>
                <label for="language">Language Spoken</label> <br />
                <br />
                <input type="checkbox" name="language[]" value="Indonesia" />Indonesia <br />
                <input type="checkbox" name="language[]" value="English" />English <br />
                <input type="checkbox" name="language[]" value="Other" />Other
            </div>
            <br />
            <div>
                <label for="bio">Bio :</label><br />
                <br />
                <textarea name="bio" id="" cols="30" rows="10"></textarea>
            </div>
            <div>
                <button type="submit">Sign Up</button>
            </div>
        </form>
    </div>
</body>

</html>
