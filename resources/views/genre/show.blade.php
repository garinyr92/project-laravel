@extends('layouts.master')

@section('title')
Genre
@endsection

@section('content-title')
Show Genre Data
@endsection

@section('content')
<div>
    <h2>Genre Data</h2>
    <div class="form-group">
        <label for="nama">Nama Genre</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{ $genre->nama }}" readonly>
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <a href="{{ url('genre') }}" class="btn btn-secondary btn-sm">Kembali</a>
</div>
@endsection
