@extends('layouts.master')

@section('title')
Genre
@endsection

@section('content-title')
Create Genre
@endsection

@section('content')
<div>
    <h2>Tambah Data</h2>
    <form action="/genre" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama Genre</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama" required>
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection
