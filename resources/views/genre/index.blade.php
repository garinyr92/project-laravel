@extends('layouts.master')

@section('title')
Genre
@endsection

@section('content-title')
List Genre
@endsection

@section('content')
<div class="float-right" style="padding-bottom: 1rem">
    <a href="/genre/create" class="btn btn-primary">Tambah</a>
</div>
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Genre</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->nama}}</td>
            <td>
                <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                <a href="/genre/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/genre/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="5" style="text-align: center">No data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection
