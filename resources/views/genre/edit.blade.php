@extends('layouts.master')

@section('title')
Genre
@endsection

@section('content-title')
Edit Genre
@endsection

@section('content')
<div>
    <h2>Edit Data</h2>
    <form action="/genre/{{$genre->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama Genre</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama"
                value="{{ $genre->nama }}" required>
            @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <a href="{{ url('genre') }}" class="btn btn-secondary btn-sm">Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </form>
</div>
@endsection
