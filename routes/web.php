<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@index');

Route::post('/welcome', 'AuthController@show');

// Route::get('/master', function () {
//     return view('layouts.master');
// });

Route::get('/items', function () {
    return view('items.index');
});
Route::get('/create', function () {
    return view('items.create');
});

// tugas
Route::get('/table', function () {
    return view('table');
});
Route::get('/data-tables', function () {
    return view('data_table');
});


// eloquent
Route::group(['middleware' => ['auth']], function () {
    //
    Route::resource('cast', 'CastController');
    Route::resource('genre', 'GenreController');
});




// Route::get('/cast', 'CastController@index');

// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');

// Route::get('/cast/{cast_id}', 'CastController@show');

// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');

// Route::delete('/cast/{cast_id}', 'CastController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
