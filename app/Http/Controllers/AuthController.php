<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function show(Request $request)
    {
        // dd($request->all());
        $name = $request->firstName. " " . $request->lastName;
        return view('welcome', ['name' => $name]);
    }

}
